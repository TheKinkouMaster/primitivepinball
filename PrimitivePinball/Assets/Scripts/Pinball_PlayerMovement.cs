﻿using UnityEngine;
using System.Collections;

public class Pinball_PlayerMovement : MonoBehaviour {
    public float  playerSpeedMax;  //Max speed that player can get
    public float  playerSpeed;     //Player speed at the moment
    public string playerDirection; //Direction on movement, useful for ball bouncing


	void Update () {
        //Conditions to move in directions
        bool canMoveLeft = FindCollision("Left");
        bool canMoveRight = FindCollision("Right");

        //Movement Left/Right
        if (Input.GetKey(KeyCode.LeftArrow) && canMoveLeft == true)
        {
            //if player is not moving or is switching moving direction he stops
            if (playerDirection == "Right" || playerDirection == "None") playerSpeed = 0;
            playerDirection = "Left";

            //if player is under his max speed then he's speeding up a little
            if (playerSpeed < playerSpeedMax) playerSpeed += 0.1f * playerSpeedMax;

            //speed is applied directly to transform
            transform.Translate(new Vector3(-playerSpeed, 0, 0));
        }
        else if (Input.GetKey(KeyCode.RightArrow) && canMoveRight == true)
        {
            if (playerDirection == "Left" || playerDirection == "None") playerSpeed = 0;
            playerDirection = "Right";

            if (playerSpeed < playerSpeedMax) playerSpeed += 0.1f * playerSpeedMax;

            transform.Translate(new Vector3(playerSpeed, 0, 0));
        }
        //if nothing hit player stops, and direction is set to "None" to prevent ball from bouncing off in wrong direction
        else playerDirection = "None";

        //Exit from keyboard
        if (Input.GetKey(KeyCode.Escape))
        {
            GameObject.FindGameObjectWithTag("GameController").GetComponent<Pinball_MainMenuScript>().EndGame();
        }
    }

    public bool FindCollision(string direction)
    {
        Vector3 lookingVector; //Vector I'm gonna use to cast ray
        RaycastHit[] stuffHit; //Table for all objects hit

        //Defining vector
        if (direction == "Left") lookingVector = new Vector3(-1, 0, 0);
        else lookingVector = new Vector3(1, 0, 0);

        //Casting ray
        stuffHit = Physics.RaycastAll(transform.position, lookingVector, transform.localScale.x/2);

        //Detecting collisions with borders of the map
        for (int i = 0; i < stuffHit.Length; i++)
        {
            //Goes off when colliding with edge
            if (stuffHit[i].transform.tag == "Edge") return false;
        }

        //Goes off only when no collision detected
        return true;
    }
}
