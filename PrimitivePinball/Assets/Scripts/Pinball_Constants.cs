﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Pinball_Constants : MonoBehaviour {
    public float  ballSpeed;              
    public float  playerSize;
    public int    numberOfBalls;
    public string currentDifficulty;

    public Text   text;

    void Start()
    {
        ChangeDifficulty("Easy"); //Setting up starting difficulty "just in case"
    }

    public void ChangeDifficulty(string newDifficulty) //Easy Medium Hard
    {
        currentDifficulty = newDifficulty;

        if      (newDifficulty == "Easy")
        {
            ballSpeed = 0.1f;
            playerSize = 20;
        }
        else if (newDifficulty == "Medium")
        {
            ballSpeed = 0.2f;
            playerSize = 15;
        }
        else if (newDifficulty == "Hard")
        {
            ballSpeed = 0.4f;
            playerSize = 10;
        }

        //Changing header
        text.text = "Level selected: " + newDifficulty;
    }
}
