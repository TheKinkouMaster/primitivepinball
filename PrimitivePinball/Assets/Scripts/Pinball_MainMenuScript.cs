﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Pinball_MainMenuScript : MonoBehaviour {
    ////////////Variables////////////
    public bool       firstStart = true;
    public int        score;
    ////////////Refs////////////
    public Text       startButton;
    public GameObject scoreText;
    public GameObject myCamera;
    public GameObject[] UIToDisableList;
    ////////////Prefabs////////////
    public GameObject playerPrefab;
    public GameObject ballPrefab;
    public GameObject boxPrefab;

    void Start()
    {
        //reseting score
        score = 0;
    }

    void Update()
    {
        if (firstStart == true)
        {
            startButton.text = "Start";
            scoreText.SetActive(false);
        }
        else startButton.text = "Restart";
    }

    public void ExitMinigame() //Goes off when user pushes Exit
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }

    public void StartGame()    //Goes off when user pushes Start/Restart
    {
        //Score reset
        score = 0;

        //Camera relocating
        myCamera.transform.position = new Vector3(0, 45, 0);

        //Menu UI disabling
        for (int i = 0; i < UIToDisableList.Length; i++)
        {
            UIToDisableList[i].SetActive(false);
        }

        //Player setup: creating -> adjusting to difficulty
        GameObject tmp = (GameObject)Instantiate(playerPrefab, new Vector3(0, 0.5f, -24.5f), transform.rotation);
        float playerSize = GetComponent<Pinball_Constants>().playerSize;
        tmp.GetComponent<Transform>().localScale = new Vector3(playerSize, 1, 1);

        //Ball setup:   creating -> adding sourceScript for easier use 
        tmp = (GameObject)Instantiate(ballPrefab, new Vector3(0, 0.5f, -10), transform.rotation);
        tmp.GetComponent<Pinball_BallScript>().sourceScript = GetComponent<Pinball_Constants>();

        //Boxes setup:  in empty space randomly places boxes
        for (int i = 0; i< 24; i++)
        {
            for (int j = 0; j < 46; j++)
            {
                if(Random.value>=0.3)
                {
                    Instantiate(boxPrefab, new Vector3(j - 22f, 0.5f, i + 0f), transform.rotation);
                }
            }
        }
    }

    public void EndGame()
    {
        //Clearing map from all objects: Player -> Ball -> Boxes
        Destroy(GameObject.FindGameObjectWithTag("Player"));
        Destroy(GameObject.FindGameObjectWithTag("Ball"));
        GameObject[] boxes = GameObject.FindGameObjectsWithTag("Box");
        for (int i = 0; i < boxes.Length; i++)
            Destroy(boxes[i]);

        //Setting up score on main menu
        scoreText.GetComponent<Text>().text = "Your score was: " + score;

        //Checking firstStart, so Start button changes into Restart
        firstStart = false;

        //Camera position change to get main menu background
        myCamera.transform.position = new Vector3(100, 105, 100);

        //Activating main Menu UI
        for (int i = 0; i < UIToDisableList.Length; i++)
        {
            UIToDisableList[i].SetActive(true);
        }
    }
}
