﻿using UnityEngine;
using System.Collections;

public class Pinball_BallScript : MonoBehaviour {
    public float             ballSpeed;
    public Pinball_Constants sourceScript;
    public Vector3           ballMovingDirection;

    public Object[] sounds;

    void Start()
    {
        //reseting ball speed, setting moving direction towards players starting position
        ballSpeed = 0;
        ballMovingDirection = new Vector3(0.05f, 0, -1);
    }

    void Update()
    {
        //Catching ballspeed from reference
        if (ballSpeed == 0) ballSpeed = sourceScript.ballSpeed;
    
        Move();
        DetectCollision();
        CheckIfBoxesOnMap(); 
        
    }

    void CheckIfBoxesOnMap() //if no boxes: simply end game
    {
        GameObject[] boxes = GameObject.FindGameObjectsWithTag("Box");
        if (boxes.Length == 0) GameObject.FindGameObjectWithTag("GameController").GetComponent<Pinball_MainMenuScript>().EndGame();
    }

    void Move()
    {
        transform.position += ballMovingDirection * ballSpeed;
    }

    void DetectCollision()
    {
        RaycastHit[] stuffHit;

        //Collision with borders
        //If it hits edge it bounces off 
        //If it hits bottom edge game ends
        stuffHit = Physics.RaycastAll(transform.position, new Vector3(1, 0, 0), 0.5f);
        for (int i = 0; i < stuffHit.Length; i++)
        {
            if(stuffHit[i].transform.tag == "Edge")
                ballMovingDirection.x = -ballMovingDirection.x;
        }
        stuffHit = Physics.RaycastAll(transform.position, new Vector3(-1, 0, 0), 0.5f);
        for (int i = 0; i < stuffHit.Length; i++)
        {
            if (stuffHit[i].transform.tag == "Edge")
                ballMovingDirection.x = -ballMovingDirection.x;
        }
        stuffHit = Physics.RaycastAll(transform.position, new Vector3(0, 0, 1), 0.5f);
        for (int i = 0; i < stuffHit.Length; i++)
        {
            if (stuffHit[i].transform.tag == "Edge")
                ballMovingDirection.z = -ballMovingDirection.z;
        }
        stuffHit = Physics.RaycastAll(transform.position, new Vector3(0, 0, -1), 0.5f);
        for (int i = 0; i < stuffHit.Length; i++)
        {
            if (stuffHit[i].transform.tag == "LoseEdge")
                GameObject.FindGameObjectWithTag("GameController").GetComponent<Pinball_MainMenuScript>().EndGame();
        }

        //Collision with player
        //Adjusting ball movement direction, which is determined by player's movement
        stuffHit = Physics.RaycastAll(transform.position, new Vector3(1, 0, -1), 0.5f);
        for (int i = 0; i < stuffHit.Length; i++)
        {
            if (stuffHit[i].transform.tag == "Player")
            {
                ballMovingDirection.z = -ballMovingDirection.z;
                if (stuffHit[i].transform.GetComponent<Pinball_PlayerMovement>().playerDirection == "Right") ballMovingDirection.x = +stuffHit[i].transform.GetComponent<Pinball_PlayerMovement>().playerSpeed;
                if (stuffHit[i].transform.GetComponent<Pinball_PlayerMovement>().playerDirection == "Left") ballMovingDirection.x = -stuffHit[i].transform.GetComponent<Pinball_PlayerMovement>().playerSpeed;
                GetComponent<AudioSource>().clip = (AudioClip) sounds[1];
                GetComponent<AudioSource>().Play();
            }
        }

        //Collision with boxes
        if (ballMovingDirection.x != 0)        //Additional condition to prevent deleting boxes that touches, but is not really hitting with speed
        {
            stuffHit = Physics.RaycastAll(transform.position, new Vector3(1, 0, 0), 0.5f);
            for (int i = 0; i < stuffHit.Length; i++)
            {
                if (stuffHit[i].transform.tag == "Box")
                {
                    ballMovingDirection.x = -ballMovingDirection.x;                                                     //Changing Direction
                    Destroy(stuffHit[i].transform.gameObject);                                                          //Removing Box
                    GameObject.FindGameObjectWithTag("GameController").GetComponent<Pinball_MainMenuScript>().score++;  //Increasing Score
                    GetComponent<AudioSource>().clip = (AudioClip)sounds[0];
                    GetComponent<AudioSource>().Play();
                    break;
                }
            }


            stuffHit = Physics.RaycastAll(transform.position, new Vector3(-1, 0, 0), 0.5f);
            for (int i = 0; i < stuffHit.Length; i++)
            {
                if (stuffHit[i].transform.tag == "Box")
                {
                    ballMovingDirection.x = -ballMovingDirection.x;
                    Destroy(stuffHit[i].transform.gameObject);
                    GameObject.FindGameObjectWithTag("GameController").GetComponent<Pinball_MainMenuScript>().score++;
                    GetComponent<AudioSource>().clip = (AudioClip)sounds[0];
                    GetComponent<AudioSource>().Play();
                    break;
                }
            }
        }
        if (ballMovingDirection.z != 0)
        {
            stuffHit = Physics.RaycastAll(transform.position, new Vector3(0, 0, 1), 0.5f);
            for (int i = 0; i < stuffHit.Length; i++)
            {
                if (stuffHit[i].transform.tag == "Box")
                {
                    ballMovingDirection.z = -ballMovingDirection.z;
                    Destroy(stuffHit[i].transform.gameObject);
                    GameObject.FindGameObjectWithTag("GameController").GetComponent<Pinball_MainMenuScript>().score++;
                    GetComponent<AudioSource>().clip = (AudioClip)sounds[0];
                    GetComponent<AudioSource>().Play();
                    break;
                }
            }
            stuffHit = Physics.RaycastAll(transform.position, new Vector3(1, 0, -1), 0.5f);
            for (int i = 0; i < stuffHit.Length; i++)
            {
                if (stuffHit[i].transform.tag == "Box")
                {
                    ballMovingDirection.z = -ballMovingDirection.z;
                    Destroy(stuffHit[i].transform.gameObject);
                    GameObject.FindGameObjectWithTag("GameController").GetComponent<Pinball_MainMenuScript>().score++;
                    GetComponent<AudioSource>().clip = (AudioClip)sounds[0];
                    GetComponent<AudioSource>().Play();
                    break;
                }
            }
        }
    }
}
